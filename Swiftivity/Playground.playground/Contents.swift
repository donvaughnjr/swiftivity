//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

public protocol Defaultable {
    init()
}
extension Int: Defaultable {}
extension Double: Defaultable {}

//func sum<N: protocol<Defaultable, IntegerArithmeticType>>(values: N...) -> N {
//    var total:N = N()
//    
//    for v in values {
//        total += v
//    }
//    
//    return total
//}
//
//sum(1.1, 2.0, 3.0, 4.0)


//func heightForSubviews<N: protocol<Defaultable, IntegerArithmeticType>>(values: N...) -> N {
//    var total:N = N()
//    
//    for v in values {
//        total += v
//    }
//    
//    return total
//}

func min<N: Comparable>(x: N, _ y: N, optionalNumbers: N...) -> N {
    var min = x < y ? x : y
    
    for number in optionalNumbers {
        if number < min {
            min = number
        }
    }
    
    return min
}

func min<N: Comparable>(numbers: [N]) -> N? {
    var min = numbers.first
    
    if numbers.count > 1 {
        for n in numbers {
            if n < min {
                min = n
            }
        }
    }
    
    return min
}

min(3, 2, 1)
min([1, 2, 3])
let test:Int? = min([])

let minNumRangeA: CGFloat = 0.75
let maxNumRangeA: CGFloat = 0.95

let minNumRangeB: CGFloat = 1
let maxNumRangeB: CGFloat = 6

let input: CGFloat = 1

1 - ((maxNumRangeB - input) / maxNumRangeB)


//static double numberInComparableRange(double range1min, double range1max, double range2min, double range2max, double numberInRange1) {
//    double range1Delta = range1max - range1min;
//    double numberInRange1Delta = numberInRange1 - range1min;
//    double percentOfRange1 = numberInRange1Delta / range1Delta;
//    double range2Delta = range2max - range2min;
//    
//    return range2min + range2Delta * percentOfRange1;
//}

protocol NumericType {
    func +(lhs: Self, rhs: Self) -> Self
    func -(lhs: Self, rhs: Self) -> Self
    func *(lhs: Self, rhs: Self) -> Self
    func /(lhs: Self, rhs: Self) -> Self
    func %(lhs: Self, rhs: Self) -> Self
    init()
}

extension Double : NumericType { }
extension Float  : NumericType { }
extension Int    : NumericType { }
extension Int8   : NumericType { }
extension Int16  : NumericType { }
extension Int32  : NumericType { }
extension Int64  : NumericType { }
extension UInt   : NumericType { }
extension UInt8  : NumericType { }
extension UInt16 : NumericType { }
extension UInt32 : NumericType { }
extension UInt64 : NumericType { }

func numberInComparableRange<N: NumericType>(range1min: N, range1max: N, range2min: N, range2max: N, numberInRange1: N) -> N {
    let range1Delta = range1max - range1min
    let numberInRange1Delta = numberInRange1 - range1min
    let percentOfRange1 = numberInRange1Delta / range1Delta
    let range2Delta = range2max - range2min
    
    return range2min + range2Delta * percentOfRange1
}
var test3 = [1,2,3]
test3.removeAtIndex(0)
while test3.count > 0 {
    let n = test3.removeAtIndex(0)
    n
}
var alpha: CGFloat = 1
let particleAlphaVariance: CGFloat = 0.75
let particleNumber = 2
alpha *= CGFloat(pow(Double(particleAlphaVariance ?? 1), Double(particleNumber-1)))

protocol MyProtocol {
}
extension MyProtocol {
    func getAnInt() -> Int {
        return 1
    }
}

class MyClassA: MyProtocol {
    
}

let protocolTest = MyClassA()
protocolTest.getAnInt()


protocolTest.getAnInt()
extension MyClassA {
    func getAnInt() -> Int {
        return 2
    }
}


