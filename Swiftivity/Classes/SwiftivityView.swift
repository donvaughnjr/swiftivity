//
//  Swiftivity.swift
//  Swiftivity
//
//  Created by Don Vaughn on 24/08/15.
//  Copyright © 2015 Don Vaughn. All rights reserved.
//

import UIKit


class SwiftivityParticleView: UIView {
}

class SwiftivityDropletParticle: SwiftivityParticleView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    override var frame: CGRect {
        didSet {
            updateCornerRadius()
        }
    }
    
    private func updateCornerRadius() {
        layer.cornerRadius = min(frame.size.width, frame.size.height) / 2
    }
    
    func setup() {
        layer.masksToBounds = true
        updateCornerRadius()
    }
    
}

@objc enum SwiftivityParticleStartPosition: Int {
    case Top = 0
    case Right
    case Bottom
    case Left
    
    init?(_ position: String) {
        switch position.lowercaseString {
        case "top": self = .Top
        case "right": self = .Right
        case "bottom": self = .Bottom
        case "left": self = .Left
        default: return nil
        }
    }
}

@objc protocol SwiftivityParticleStyleType {
    
    var indicator: SwiftivityView  { get set }
    
    init(indicator: SwiftivityView)
    
    func setup()

    func baseSize() -> CGFloat
    
    func timingFunction(particleNumber: Int) -> CAMediaTimingFunction
    
    func particle(particleNumber: UInt) -> SwiftivityParticleView
    
    func particles() -> [SwiftivityParticleView]
    
    optional func prepareForInterfaceBuilder()
    
    optional func isAnimating() -> Bool
    
    optional func startAnimating()
    
    optional func stopAnimating()
    
    optional func indicatorIntrinsicContentSize() -> CGSize
    
}

class SwiftivityViewDefaults {
    class func radius() -> CGFloat { return 0 }
    class func loopDuration() -> CGFloat { return 1.8 }
    class func startPosition() -> SwiftivityParticleStartPosition { return .Top }
    class func fadeDuration() -> CGFloat { return 0.3 }
    class func particleCount() -> UInt { return 4 }
    class func particleSize() -> CGFloat { return 0 }
    class func particleSizeVariance() -> CGFloat { return 1 }
    class func particleAlpha() -> CGFloat { return 1.0 }
    class func particleAlphaVariance() -> CGFloat { return 1 }
    class func particleProportion() -> CGFloat { return 0.15 }
}

@objc class SwiftivityParticleStyleDroplet: NSObject, SwiftivityParticleStyleType {
    
    // Mark: - Properties
    
    var indicator: SwiftivityView
    var animationDelayLinearGrowth: CFTimeInterval = 0.075
    var animationDelayIterativeGrowth: CFTimeInterval = 0.02
    var animationDurationLinearDecline: CFTimeInterval = 0.025
    var animationDurationIterativeDecline: CFTimeInterval = 0.00
    var minParticleScaling: CGFloat = 0.65
    var maxParticleScaling: CGFloat = 1.5
    var prominentParticleRate: CGFloat = 0.25 {
        didSet {
            prominentParticleRate = min(1, max(0, prominentParticleRate))
        }
    }
    var particleDropoffRate: CGFloat = 0.5 {
        didSet {
            prominentParticleRate = min(1, max(0, prominentParticleRate))
        }
    }
    
    internal let animationKeypath = "spinner"
    
    internal var _isAnimating = false
    
    var smallestParticle: SwiftivityParticleView? {
        return indicator.subviews.reduce(indicator.subviews.first, combine: {
            if $0 == nil {
                return $1
            }
            return ($0!.frame.size.width < $1.frame.size.width) ? $0 : $1
        }) as? SwiftivityParticleView
    }
    
    var largestParticle: SwiftivityParticleView? {
        return indicator.subviews.reduce(indicator.subviews.first, combine: {
            if $0 == nil {
                return $1
            }
            return ($0!.frame.size.width > $1.frame.size.width) ? $0 : $1
        }) as? SwiftivityParticleView
    }
    
    // Mark: - Init
    
    required init(indicator: SwiftivityView) {
        self.indicator = indicator
    }
    
    func setup() {
//        indicator.backgroundColor = UIColor.blueColor()
        
        indicator.subviews.forEach { $0.removeFromSuperview() }
    }
    
    func addParticlesToView() -> [SwiftivityParticleView] {
        let _particles = particles()
        
        _particles.forEach {
            self.indicator.addSubview($0)
            $0.frame = frameForParticleStartPosition($0)
        }
        
        return _particles
    }
    
    override func prepareForInterfaceBuilder() {
//        indicator.backgroundColor = UIColor.yellowColor()
        
        indicator.hidden = false
        indicator.alpha = 1
        startAnimating()
        
        var lastPosition: CGFloat = 0
        
        var index: Double = 0
        indicator.subviews.forEach {
            $0.hidden = false
            $0.frame = CGRectMake(
                $0.frame.origin.x,
                $0.frame.origin.y + lastPosition,
                //                CGRectGetMidX(frame) + $0.frame.size.width/2 * CGFloat(cos((3 + index) * M_PI_2)),
                //                CGRectGetMidY(frame) + $0.frame.size.height/2 * CGFloat(sin((7 + index) * M_PI_2)),
                $0.frame.size.width,
                $0.frame.size.height
            )
            lastPosition += 10
            index += 1
        }
    }
    
    // Mark: - Style methods
    
    func timingFunction(particleNumber: Int) -> CAMediaTimingFunction {
        return CAMediaTimingFunction(controlPoints:
            0.74,
            0.18,
            0.0,
            numberInComparableRange(
                Float(1),
                range1max: Float(indicator.particleCount),
                range2min: Float(0.76),
                range2max: Float(0.94),
                numberInRange1: Float(particleNumber)
            )
        )
    }
    
    func baseSize() -> CGFloat {
        let frame = indicator.frame
        let size = indicator.particleSize
        
        return size > 0 ? size : min(frame.size.width, frame.size.height) * indicator.particleProportion
    }
    
    func particle(particleNumber: UInt) -> SwiftivityParticleView {
        let particle = SwiftivityDropletParticle()
        var particleSize = baseSize()
        var alpha:CGFloat = 1
        
        if particleNumber > 1 {
            particleSize *= CGFloat(pow(Double(indicator.particleSizeVariance ?? 1), Double(particleNumber)))
            alpha *= CGFloat(pow(Double(indicator.particleAlphaVariance ?? 1), Double(particleNumber-1)))
        }
        
        particle.backgroundColor = indicator.color
        particle.bounds = CGRectMake(0, 0, particleSize, particleSize)
        particle.frame = particle.bounds
        particle.alpha = alpha
        
        return particle
    }
    
    func particles() -> [SwiftivityParticleView] {
        var particles = [SwiftivityParticleView]()
        
        (1...indicator.particleCount).forEach {
            particles.append(self.particle($0))
        }
        
        return particles
    }
    
    // Mark: - Indicator Methods
    
    func startAnimating() {
        if !_isAnimating {
            _isAnimating = true
            indicator.hidden = false
            UIView.animateWithDuration(CFTimeInterval(indicator.fadeDuration)) {
                self.indicator.alpha = 1
            }
        }
        
        
        var duration = CFTimeInterval(indicator.loopDuration)
        var delay: CFTimeInterval = 0
        let newParticles = addParticlesToView()
        let lastProminenParticleIndex = Int(CGFloat(indicator.particleCount) * prominentParticleRate)
        
        for particleIndex in 0..<newParticles.count {
            let particle = newParticles[particleIndex] 
            let rotationAnimation = CAKeyframeAnimation(keyPath: "position")
            let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
            let alphaAnimation = CAKeyframeAnimation(keyPath: "opacity")
            let groupAnimation = CAAnimationGroup()

            let midParticleScaling = minParticleScaling + ((maxParticleScaling - minParticleScaling) / 2)
            let minScaling = (particle.frame.size.width * minParticleScaling) / particle.frame.size.width
            let midScaling = (particle.frame.size.width * midParticleScaling) / particle.frame.size.width
            let maxScaling = (particle.frame.size.width * maxParticleScaling) / particle.frame.size.width
            
            rotationAnimation.path = UIBezierPath(
                arcCenter: animationPointOffsetForParticleStartPosition(particle),
                radius: indicator.intrinsicRadius - particle.frame.size.width/2,
                startAngle: CGFloat(Double(3 + indicator.startPosition.rawValue) * M_PI_2),
                endAngle: CGFloat(Double(7 + indicator.startPosition.rawValue) * M_PI_2),
                clockwise: true).CGPath
            rotationAnimation.additive = true
            
            if particleIndex < lastProminenParticleIndex {
                scaleAnimation.keyTimes = [0, 0.35, 0.7, 0.75, 0.9, 1]
                scaleAnimation.values = [
                    1.35,
                    0.75,
                    maxScaling,
                    1.5,
                    1.0,
                    1.35
                ]
                
            } else {
                scaleAnimation.keyTimes = [0, 0.2, 0.3, 0.9, 1]
                scaleAnimation.values = [
                    1,
                    minScaling,
                    midScaling,
                    minScaling,
                    1
                ]
                
                alphaAnimation.keyTimes = [0, 0.25, 0.75, 1]
                alphaAnimation.values = [
                    particle.alpha,
                    particle.alpha,
                    0,
                    0
                ]
            }
            scaleAnimation.duration = duration
            alphaAnimation.duration = duration
            
            rotationAnimation.duration = duration
            rotationAnimation.timingFunction = timingFunction(particleIndex + 1)
            rotationAnimation.fillMode = kCAFillModeBackwards
            
            groupAnimation.animations = [rotationAnimation, alphaAnimation, scaleAnimation]
            groupAnimation.duration = duration
            groupAnimation.beginTime = CACurrentMediaTime() + delay
            groupAnimation.fillMode = kCAFillModeBackwards
            groupAnimation.delegate = self
            groupAnimation.removedOnCompletion = false
            groupAnimation.setValue(particle, forKey: "swiftivityParticleView")
            groupAnimation.setValue(particleIndex, forKey: "swiftivityParticleViewIndex")
            
            delay += animationDelayLinearGrowth + (animationDelayIterativeGrowth * Double(particleIndex))
            duration -= animationDurationLinearDecline - (animationDurationIterativeDecline * Double(particleIndex))
            
            particle.layer.addAnimation(groupAnimation, forKey: animationKeypath)
        }
    }
    
    func isAnimating() -> Bool {
        return _isAnimating
    }
    
    func stopAnimating() {
        _isAnimating = false
        
        UIView.animateWithDuration(
            CFTimeInterval(indicator.hidesWhenStopped ? indicator.fadeDuration : 0),
            animations: { () -> Void in
                if self.indicator.hidesWhenStopped {
                    self.indicator.alpha = 0
                }
                
            }, completion: { (animationDidFinish) -> Void in
                self.indicator.subviews.forEach { $0.layer.removeAllAnimations() }
                if self.indicator.hidesWhenStopped {
                    self.indicator.hidden = true
                }
        })
    }
    
    override func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        let particle = anim.valueForKey("swiftivityParticleView") as! SwiftivityParticleView
        let isFirstParticle = (anim.valueForKey("swiftivityParticleViewIndex") as! Int) == 0
        
        particle.layer.removeAllAnimations()
        particle.removeFromSuperview()
        
        if isFirstParticle && isAnimating() {
            self.startAnimating()
        }
    }
    
    // Mark: - Helpers
    
    private func frameForParticleStartPosition(particle: SwiftivityParticleView) -> CGRect {
        switch indicator.startPosition {
        case .Top: return CGRectMake(
            indicator.intrinsicContentSize().width/2 - particle.frame.size.width/2,
            largestParticle!.frame.size.height/2 - particle.frame.size.height/2,
            particle.frame.size.width,
            particle.frame.size.height
            )
        case .Right: return CGRectMake(
            indicator.intrinsicContentSize().width - particle.frame.size.width/2 - largestParticle!.frame.size.width/2,
            indicator.intrinsicContentSize().height/2 - particle.frame.size.height/2,
            particle.frame.size.width,
            particle.frame.size.height
            )
        case .Bottom: return CGRectMake(
            indicator.intrinsicContentSize().width/2 - particle.frame.size.width/2,
            indicator.intrinsicContentSize().height - largestParticle!.frame.size.height + largestParticle!.frame.size.height/2 - particle.frame.size.height/2,
            particle.frame.size.width,
            particle.frame.size.height
            )
        case .Left: return CGRectMake(
            largestParticle!.frame.size.width/2 - particle.frame.size.width/2,
            indicator.intrinsicContentSize().height/2 - particle.frame.size.height/2,
            particle.frame.size.width,
            particle.frame.size.height
            )
        }
    }
    
    private func animationPointOffsetForParticleStartPosition(particle: SwiftivityParticleView) -> CGPoint {
        switch indicator.startPosition {
        case .Top:
            return CGPointMake(
                0,
                indicator.intrinsicRadius - particle.frame.size.height/2
            )
        case .Right:
            return CGPointMake(
                particle.frame.size.width/2 - indicator.intrinsicRadius,
                indicator.intrinsicRadius - indicator.intrinsicContentSize().height/2
            )
        case .Bottom:
            return CGPointMake(
                0,
                particle.frame.size.height/2 - indicator.intrinsicRadius
            )
        case .Left:
            return CGPointMake(
                indicator.intrinsicContentSize().width/2 - particle.frame.size.width/2,
                0
            )
        }
    }
    
}

@objc protocol SwiftivityViewType {
    
    var forInterfaceBuilder: Bool { get }
    
    // This is the class that will implement the functionality of the swiftivity view (e.g., the
    // setup, setupAnimation, startAnimating, etc. methods).
    var particleStyle: SwiftivityParticleStyleType { get set }

    var startPosition: SwiftivityParticleStartPosition { get set }
    
    var radius: CGFloat { get set }
    
    var fadeDuration: CGFloat { get set }
    
    var loopDuration: CGFloat { get set }
    
    var particleCount: UInt { get set }

    var particleProportion: CGFloat { get set }

    var particleSize: CGFloat { get set }

    var particleSizeVariance: CGFloat { get set }

    var particleAlpha: CGFloat { get set }
    
    var particleAlphaVariance: CGFloat { get set }
    
}

@IBDesignable class SwiftivityView: UIActivityIndicatorView, SwiftivityViewType {
    
    typealias Defaults = SwiftivityViewDefaults
    
    // Mark: - Properties
    
    private(set) var forInterfaceBuilder = false
    
    @IBInspectable var radius: CGFloat = Defaults.radius() {
        didSet {
            invalidateIntrinsicContentSize()
            setup()
        }
    }
    var intrinsicRadius: CGFloat {
        return radius > 0 ? radius : intrinsicContentSize().width / 2
    }
    
    @IBInspectable var startPosition = Defaults.startPosition()
    @IBInspectable var startPositionName: String {
        get {
            return "\(startPosition.rawValue)"
        }
        set {
            let normalizedNewValue = newValue.lowercaseString.stringByReplacingOccurrencesOfString(" ", withString: "")
            
            if let newPosition = SwiftivityParticleStartPosition(normalizedNewValue) {
                startPosition = newPosition
                setup()
            }
        }
    }
    
    @IBInspectable var fadeDuration: CGFloat = Defaults.fadeDuration()
    
    @IBInspectable var loopDuration: CGFloat = Defaults.loopDuration()
    
    lazy var _particleStyle: SwiftivityParticleStyleType = SwiftivityParticleStyleDroplet(indicator: self)
    var particleStyle: SwiftivityParticleStyleType {
        get {
            return _particleStyle
        }
        set {
            _particleStyle = newValue
            setup()
        }
    }
    @IBInspectable var particleStyleName: String {
        get {
            return "\(self.particleStyle.dynamicType)"
        }
        set  {
            if let newParticleStyle = NSClassFromString("ParticleIndicatorStyle\(newValue.capitalizedString)") as? SwiftivityParticleStyleType {
                _particleStyle = newParticleStyle
                setup()
            }
        }
    }
    
    @IBInspectable var particleCount: UInt = Defaults.particleCount() {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var particleProportion: CGFloat = Defaults.particleProportion() {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var particleSize: CGFloat = Defaults.particleSize() {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var particleSizeVariance: CGFloat = Defaults.particleSizeVariance() {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var particleAlpha: CGFloat = Defaults.particleAlpha() {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var particleAlphaVariance: CGFloat = Defaults.particleAlphaVariance() {
        didSet {
            setup()
        }
    }
    
    // Mark: - Init
    
    override init(activityIndicatorStyle style: UIActivityIndicatorViewStyle) {
        super.init(activityIndicatorStyle: style)
        setup()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        _particleStyle.setup()
    }
    
    override func prepareForInterfaceBuilder() {
        forInterfaceBuilder = true
        
        setup()
        _particleStyle.prepareForInterfaceBuilder?() ?? super.prepareForInterfaceBuilder()
    }
    
    override func isAnimating() -> Bool {
        return _particleStyle.isAnimating?() ?? super.isAnimating()
    }
    
    override func startAnimating() {
        _particleStyle.startAnimating?() ?? super.startAnimating()
    }
    
    override func stopAnimating() {
        _particleStyle.stopAnimating?() ?? super.stopAnimating()
    }
    
    override func intrinsicContentSize() -> CGSize {
        if let indicatorIntrinsicContentSize = _particleStyle.indicatorIntrinsicContentSize {
            return indicatorIntrinsicContentSize()
            
        } else if radius > 0 {
            return CGSizeMake(radius*2, radius*2)
            
        } else {
            return super.intrinsicContentSize()
        }
    }
    
}


// ------------------------------------------------------------------------------------------------------------

//@IBDesignable class SwiftyIndicator: Swifty {
//    
//    var activityIndicatorView = SwiftivityView()
//    
//    @available(*, unavailable, message="This property is reserved for Interface Builder. Use activityIndicatorView.hidesWhenStopped instead.")
//    @IBInspectable var hidesWhenStopped: Bool {
//        get {
//            return activityIndicatorView.hidesWhenStopped
//        }
//        set {
//            activityIndicatorView.hidesWhenStopped = newValue
//        }
//    }
//    
//    @available(*, unavailable, message="This property is reserved for Interface Builder. Use activityIndicatorView.color instead.")
//    @IBInspectable var color: UIColor? {
//        get {
//            return activityIndicatorView.color
//        }
//        set {
//            activityIndicatorView.color = newValue
//        }
//    }
//    
//    @available(*, unavailable, message="This property is reserved for Interface Builder. Use activityIndicatorView.color instead.")
//    @IBInspectable var isLarge: Bool {
//        get {
//            return activityIndicatorView.activityIndicatorViewStyle == .WhiteLarge
//        }
//        
//        set {
//            activityIndicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyle(rawValue: newValue ? 0 : 1)!
//        }
//    }
//    
//    override func setupSwiftySubviews() {
//        super.setupSwiftySubviews()
//        self.addSubview(activityIndicatorView)
//        NSLog("setupSwiftySubviews SwiftyIndicator: \(self.subviews)")
//    }
//    
//}

//@objc enum SwiftyLabelPosition: Int, CustomStringConvertible {
//    case Above
//    case Centered
//    case Below
//    
//    init?(_ position: String) {
//        switch position.lowercaseString {
//        case "above": self = .Above
//        case "centered": self = .Centered
//        case "below": self = .Below
//        default: return nil
//        }
//    }
//    
//    var description: String {
//        get {
//            switch self {
//            case .Above: return "SwiftyLabelPosition.Above"
//            case .Centered: return "SwiftyLabelPosition.Centered"
//            case .Below: return "SwiftyLabelPosition.Below"
//            }
//        }
//    }
//}

//@IBDesignable class SwiftyLabelledIndicator: SwiftyIndicator {
//    
//    let defaultLabelMargin = 8
//    
//    var label = UILabel()
//    var labelPosition = SwiftyLabelPosition.Below
//    
//    @available(*, unavailable, message="This property is reserved for Interface Builder. Use label.font.fontWithSize instead.")
//    
//    @available(*, unavailable, message="This property is reserved for Interface Builder. Use label.text instead.")
//    @IBInspectable var labelText: String? {
//        get {
//            return label.text
//        }
//        set {
//            label.text = newValue
//        }
//    }
//    
//    @IBInspectable var labelOffset: CGFloat?
//    
//    @IBInspectable var fontSize: CGFloat {
//        get {
//            return label.font.pointSize
//        }
//        
//        set {
//            label.font = label.font.fontWithSize(newValue)
//        }
//    }
//    
//    @available(*, unavailable, message="This property is reserved for Interface Builder. Use label.font instead.")
//    @IBInspectable var fontName: String? {
//        get {
//            return label.font.familyName
//        }
//        set {
//            if let familyName = newValue {
//                label.font = UIFont(name: familyName, size: label.font.pointSize)
//            }
//        }
//    }
//    
//    @available(*, unavailable, message="This property is reserved for Interface Builder. Use labelPosition instead.")
//    @IBInspectable var labelPositioning: String {
//        get {
//            return "\(labelPosition.description)"
//        }
//        set {
//            let normalizedNewValue = newValue.lowercaseString.stringByReplacingOccurrencesOfString(" ", withString: "")
//            
//            if let newPosition = SwiftyLabelPosition(normalizedNewValue) {
//                labelPosition = newPosition
//            }
//        }
//    }
//    
//    override func setupSwiftySubviews() {
//        super.setupSwiftySubviews()
//        self.addSubview(label)
//        self.insertSubview(label, belowSubview: activityIndicatorView)
//        NSLog("setupSwiftySubviews SwiftyLabelledIndicator: \(self.subviews)")
//    }
//    
//    //    private func labelOffsetForCurrentLabel() -> CGFloat {
//    //        return intrinsicContentSize().height == 0 ? 0 : labelOffset;
//    //    }
//}



protocol NumericType {
    func +(lhs: Self, rhs: Self) -> Self
    func -(lhs: Self, rhs: Self) -> Self
    func *(lhs: Self, rhs: Self) -> Self
    func /(lhs: Self, rhs: Self) -> Self
    func %(lhs: Self, rhs: Self) -> Self
    init(_ v: Int)
}

extension Double : NumericType { }
extension Float  : NumericType { }
extension Int    : NumericType { }
extension Int8   : NumericType { }
extension Int16  : NumericType { }
extension Int32  : NumericType { }
extension Int64  : NumericType { }
extension UInt   : NumericType { }
extension UInt8  : NumericType { }
extension UInt16 : NumericType { }
extension UInt32 : NumericType { }
extension UInt64 : NumericType { }
extension CGFloat : NumericType { }

func numberInComparableRange<N: NumericType>(range1min: N, range1max: N, range2min: N, range2max: N, numberInRange1: N) -> N {
    let range1Delta = range1max - range1min
    let numberInRange1Delta = numberInRange1 - range1min
    let percentOfRange1 = numberInRange1Delta / range1Delta
    let range2Delta = range2max - range2min
    
    return range2min + range2Delta * percentOfRange1
}

