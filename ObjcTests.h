//
//  ObjcTests.h
//  Swiftivity
//
//  Created by Don Vaughn on 24/08/15.
//  Copyright © 2015 Don Vaughn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjcTests : NSObject

+(void)run;

@end
