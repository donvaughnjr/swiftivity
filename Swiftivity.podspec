Pod::Spec.new do |s|
  s.name         = "Swiftivity"
  s.version      = "0.0.2"
  s.summary      = "A droplet style spinner supporting storyboard-inspectable properties"
  s.description  = <<-DESC
			This is a private pod
                   DESC
  s.homepage     = "http://example.com/Swiftivity"
  s.license  = 'MIT'
  s.author             = { "Don Vaughn" => "don.vaughn.online@gmail.com" }
  s.platform     = :ios, "7.0"
  s.ios.deployment_target = '7.0'
  s.source       = { :git => "http://example.com/Swiftivity.git", :tag => "v#{s.version}" }
  s.source_files  = "Swiftivity/Classes/**/*.{h,m,swift}"
  s.header_dir   = 'Swiftivity'
  s.framework  = "UIKit"
  s.requires_arc = true
end
